<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('admin/','PersonController@index');
//Route::post('admin/store','PersonController@store');
Route::resource('admin/','PersonController');
Route::get('admin/edit/{id}','PersonController@edit');
Route::post('admin/update/{id}','PersonController@update');
Route::get('admin/destroy/{id}','PersonController@destroy');

Route::get('admin/trash','PersonController@trash');
Route::post('admin/','AssignCourseController@store');

Auth::routes();
Route::get('home','HomeController@index');
Route::get('view','HomeController@view');


Route::get('admin/getPDF','PDFController@getPDF');

//upload single file
Route::post('admin/store','ImageUploadController@store');
//upload multiple file
Route::post('admin/multipleImage','ImageUploadController@multipleImage');
Route::post('admin/email','EmailController@email');
Route::get('admin/inserData','DataInsertController@index');
Route::post('admin/inserData/','DataInsertController@store');
Route::post('admin/inserData/','DataInsertController@addAuthor');


Route::get('admin/loadData','LoadDataAjaxController@index');

Route::post('admin/loadData/load','LoadDataAjaxController@load');

Route::get('admin/InfiniteScrollAjax/','InfiniteScrollController@index');
Route::post('admin/InfiniteScrollAjax/getData','InfiniteScrollController@getData');

Route::get('admin/SearchTypeHead','SearchController@index');
Route::get('admin/SearchTypeHead/search','SearchController@search');
Route::get('admin/SearchTypeHead/getAllImage','SearchController@getAllImage');


//Route::resource('admin/project', 'ProjectController');
//Route::post('admin/project/store', 'ProjectController@store');
//Route::get('admin/project/getDataWithAjax', 'ProjectController@getDataWithAjax');

Route::get('admin/project',[
    'as'=>'project',
    'uses'=>'ProjectController@index'
]);

Route::get('admin/project/datas','ProjectController@datas')->name('datas');
Route::post('admin/project/','ProjectController@sendName')->name('sendName');



//Route::get('admin/project',[
//   'as'=>'project',
//   'uses'=>'ProjectController@index',
//]);


//Route::get('admin/','AdminController@index');

//Route::get('admin','Admin\LoginController@showLoginForm')->name('admin.login');
//Route::get('admin-password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');

Route::post('admin/','PersonController@getTime')->name('time');