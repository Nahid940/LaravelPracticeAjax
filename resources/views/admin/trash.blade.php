@extends('admin.layout.master')
@section('trash')

    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Deleted at</th>
                <th>Restore</th>
            </tr>
            @foreach($person as $p)
        <tr>
            <td>{{$p->name}}</td>
            <td>{{$p->age}}</td>
            <td>{{$p->deleted_at}}</td>
            <td><a href="" class="btn btn-warning">Restore</a></td>
        </tr>
            @endforeach
        </thead>
    </table>
@endsection