@extends('admin.layout.master')

@section('content')

    <label>Search Employee Details</label>
    <div id="search_area">
        <input type="text" name="employee_search" id="employee_search" class="form-control input-lg" autocomplete="off" placeholder="Type Employee Name" />
    </div>

@endsection

@section('script')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
    <script>

        $(document).ready(function () {


        });

        $( function() {
            $( "#employee_search" ).autocomplete({
                source: '/admin/SearchTypeHead/search'
            });
        } );

    </script>
@endsection