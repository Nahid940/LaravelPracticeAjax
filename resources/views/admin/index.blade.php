@extends('admin.layout.master')

@section('content')

    <h2>This is index page</h2>

    @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
    @endforeach

    @if(session('insert'))
        <div class="alert alert-success">{{session('insert')}}</div>
    @endif

    @if(session('update'))
        <div class="alert alert-success">{{session('update')}}</div>
    @endif

    @if(session('deleted'))
        <div class="alert alert-warning">{{session('deleted')}}</div>
    @endif

    <form action="{{url('admin/')}}" method="post">
        {{csrf_field()}}

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input type="text" name="age" class="form-control">
        </div>

        <input type="submit" value="Add details" class="btn btn-success">
    </form>


    <hr>
    <h3>Assign course</h3>
    <form action="{{url('admin/')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Student name</label>
            @if(count($student)==0)

                <h2>Nothing to assign</h2>
            @else

            <select class="form-control" id="sel1" name="studentid">
                @foreach($student as $std)

                    <option value="{{$std->id}}">{{$std->student_name}}</option>

                @endforeach
            </select>
            @endif

        </div>
        <div class="form-group">
            <label for="age">Select course</label>
            @foreach($course as $crs)
            <div class="checkbox">
                <label><input type="checkbox" value="{{$crs->id}}" name="course[]">{{$crs->cname}}</label>
            </div>
            @endforeach
        </div>
        <input type="submit" value="Add details" class="btn btn-success">
    </form>

    <hr>
    <h3>Image upload</h3>
    <form action="{{url('admin/store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" name="image">
        </div>
        <input type="submit" value="Add details" class="btn btn-success">
    </form>


    <hr>
    <h3>Multiple Image upload</h3>
    <form action="{{url('admin/multipleImage')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" name="image[]" multiple>

        </div>
        <input type="submit" value="Add details" class="btn btn-success">
    </form>


    <hr>
    <h3>Send mail</h3>
    <form action="{{url('admin/email')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="">Send email</label>
            <input type="text" class="form-control" name="phone" placeholder="phone">
            <input type="text" class="form-control" name="emailaddress" placeholder="emailaddress">
        </div>
        <input type="submit" value="Add details" class="btn btn-success">
    </form>


@endsection

@section('table')
    <h2>Data table</h2>
    <table class="table">
        <thead>
            <tr>
                <th>Sl.</th>
                <th>Name</th>
                <th>Age</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>

        <tbody>
        @php
            $i=1;
        @endphp

        @foreach($person as $p)
            <tr>
                <td>{{$i++}}</td>
                <td>{{$p->name}}</td>
                <td>{{$p->age}}</td>
                <td><a href="{{url('admin/edit/'.$p->id)}}" class="btn btn-info">Edit</a></td>
                <td><a href="{{url('admin/destroy/'.$p->id)}}" class="btn btn-danger">Delete</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    
    
    <table class="table">
        <tr>
            @php
                //$image=\App\Image::all();
            //dd($image);
            @endphp

            @foreach($data as $img)

            <td>
                <img src="{{asset('storage/image/'.$img->image)}}" alt="" height="100px" width="100px">
            </td>
            @endforeach
        </tr>
    </table>



    @php
            date_default_timezone_set("Asia/Dhaka");
            //echo "The time is " . date("h:i:sa");
    //for($i=date('i');$i<10;$i++){
    //    $data[]=date('h:i:s');
   // }
   // print_r($data);
   // $start=date('h');
    //for($i=1;$i<=5;$i++){
    //    echo date('h').':'.date('i')."<br>";
   // }

    @endphp

    <form action="{{route('time')}}" method="post">
        {{csrf_field()}}
        <input type="text" value="{{date('h:i:s')}}" name="time">
        <input type="date" value="{{date('y-m-d')}}" name="date">
        <input type="submit" value="Submit">
    </form>
@endsection