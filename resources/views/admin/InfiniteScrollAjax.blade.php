@extends('admin.layout.master')

@section('content')

    <h2>Content</h2>

    <div class="col-md-12" id="post-data">
        @foreach($datas as $d)
            <div>
                <h3><a href="">{{$d->id}}{{ $d->name }}</a></h3>
                <div class="text-right">
                    <button class="btn btn-success">Read More</button>
                </div>
                <hr style="margin-top:5px;">
            </div>
        @endforeach
            <span id='lastRow' data-id='{{$d->id}}'></span>
        <span id="loading"></span>
    </div>

@endsection



@section('script')
    <script>
        $(document).ready(function () {

            var page = 1;
            var SpanId=parseInt($('#lastRow').data('id'));

            $(window).scroll(function () {

                if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                    $('#loading').html("Loading <i class='fa fa-refresh fa-spin' style='font-size:24px'></i>");
                    loadMoreData();
                    SpanId+=10;
                }
                function loadMoreData() {
                    $.ajax(
                        {
                            url: 'InfiniteScrollAjax/getData',
                            type: "post",
                            data: {
                                page: page,
                                SpanId:SpanId,
                                _token: "{{csrf_token()}}"
                            },
                            success: (function (data) {
                                $("#post-data").append(data);
                                $('#loading').remove();

                            }),
                            beforeSend: function () {
                                //$('.post-data').show();
                            }
                        })
                }

//                        .done(function (data) {
//                            if (data.html == " ") {
//                                //$('#post-data').html("No more records found");
//                                return;
//                            }
//                            //$('#post-data').hide();
//                            $("#post-data").append(data.html);
//                        }),
//                        .fail(function (jqXHR, ajaxOptions, thrownError) {
//                            alert('server not responding...');
//                        });
//                }
            })
        })
    </script>
@endsection