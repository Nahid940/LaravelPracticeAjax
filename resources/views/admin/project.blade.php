@extends('admin.layout.master')

@section('content')
    <h3>Add Project </h3>
    <form action="{{url('admin/project/store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}

        <div class="form-group">
            <label for="">Project name</label>
            <input type="text" class="form-control" name="pname">
        </div>

        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" name="image[]" multiple>
        </div>
        <input type="submit" value="Add details" class="btn btn-success">
    </form>


    <hr>

    <table class="table">
        <tr>
            <td>Project Name</td>
            <td>Crated on</td>
            <td>Image</td>
        </tr>

    @foreach($project as $p)
           <tr>
               <td>{{$p->project->pname}}</td>
               <td>{{($p->project->created_at)->diffForHumans()}}</td>
{{--               <td><img src="{{asset('storage/pimage/'.$p->image)}}" alt=""></td>--}}
           </tr>

    @endforeach
    </table>


    <div id="myData">
        
    </div>



    <div>
        <form action="{{route('sendName')}}" method="post">
            {{csrf_field()}}
            <select id="country" style="width:500px" name="name">
                <!-- Dropdown List Option -->
            </select>
            <input type="submit" value="Submit">
        </form>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {

//            $.ajax({
//                url:'project/getDataWithAjax',
//                method:'get',
//                success:function (data) {
//                    $('#myData').append(data);
//
//                }
//            });

            var country;
            $.ajax({
                url:'{{route('datas')}}',
                method:'get',
                success:function (data) {
                    $("#country").select2({
                            data: data
                    });
                }
            });
        })

        $(document).on('click','.click',function () {
            var id=parseInt(($(this).data('id')));
//            alert(id);
            $('#like'+id).html("<i class='fa fa-thumbs-up' aria-hidden='true'></i>");
        })


    </script>
@endsection