@extends('admin.layout.master')

@section('content')

    <form action="{{url('admin/update/'.$person->id)}}" method="post">
        {{csrf_field()}}


        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" value="{{$person->name}}">
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input type="text" name="age" class="form-control" value="{{$person->age}}">
        </div>

        <input type="submit" value="Update details" class="btn btn-success">
    </form>
@endsection