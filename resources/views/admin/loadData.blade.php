@extends('admin.layout.master')

@section('content')

    <div id="load_data">

        @foreach($data as $d)
            <div class='alert alert-success'><p>{{$d->id}}{{$d->name}}</p></div>
        @endforeach

        <div id="remove-row">
            <button id="loadMoreData" data-id="{{ $d->id }}" class="btn btn-info" > Load More </button>
        </div>
    </div>

@endsection

@section('script')
    <script>

        $(document).ready(function () {
            //alert("ok");
            var token='{{\Illuminate\Support\Facades\Session::token()}}';

            var limit=7;
            var start=0;
            var action='inactive';

            $(document).on('click','#loadMoreData', function () {

                //alert($(this).data('id'));
                $("#loadMoreData").html("<i class='fa fa-spinner' aria-hidden='true'></i>");

                $.ajax({
                    url: 'loadData/load',
                    method: 'POST',
                    data: {
                        _token: token,
//                    limit: limit,
//                    start: start
                        id:$(this).data('id')
                    },
                    dataType : "text",
                    success: function (data) {
//                        for (var i = 0; i <= data.length; i++) {
//                            $('#load_data').append("<div class='jumbotron'>" + data[i]['name'] + "</div>");
//                        }
//                        if (data == '') {
//                            action = 'active';
//                        }else{
//                            action='inactive';
//                        }
//                        console.log(data);
                        if(data != '')
                        {
                            $('#remove-row').remove();
                            $('#load_data').append(data);

                        }

                    }

                });

            });












            //loadData();
            
           // function loadData(limit,start) {

//                $.ajax({
//                    url: 'loadData/load',
//                    method: 'POST',
//                    data: {
//                        _token: token,
//                        limit: limit,
//                        start: start
//                    },
//                    success: function (data) {
////                        for (var i = 0; i <= data.length; i++) {
////                            $('#load_data').append("<div class='jumbotron'>" + data[i]['name'] + "</div>");
////                        }
////                        if (data == '') {
////                            action = 'active';
////                        }else{
////                            action='inactive';
////                        }
////                        console.log(data);
//                        $('#load_data').append(data);
//                    }
//
//                });
        //    }

//            if(action == 'inactive')
//            {
//                action = 'active';
//                loadData(limit, start);
//            }

//            $(window).scroll(function () {
//                if($(window).scrollTop()+ $(window).height()>= $('#load_data').height() && action=='inactive'){
//
//                    alert("ok");
//                    action="active";
//                    start=start+limit;
//                    setTimeout(function () {
//                        loadData(start,limit);
//                    },1000);
//
//                }
//            });

        })
    </script>
@endsection