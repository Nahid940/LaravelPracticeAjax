@extends('admin.layout.master')

@section('content')

    <table class="table">
        <tr><td>Book name</td>
        <td>Author</td>
        </tr>

        @php

        @endphp

        @foreach($author as $d)
            <tr>
                <td>{{$d->bookName}}</td>
                <td>{{$d->author_name}}</td>
            </tr>
        @endforeach



    </table>


    <hr>
    <h3>This is data insert page</h3>

    @if($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">{{$error}}</div>
        @endforeach

    @endif

    <form action="{{url('admin/inserData/')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Book name</label>
            <input type="text" name="bookName" class="form-control">
        </div>
        <input type="submit" value="Add Book" class="btn btn-success">
    </form>


    <hr>

    <form action="{{url('admin/inserData/')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Book name</label>
            @php
                $books=\App\Book::get();
            //dd($books);
            @endphp
                <select name="book_id" id="" class="form-control">
                    @foreach($books as $book)
                        <option value="{{$book->id}}">{{$book->bookName}}</option>
                    @endforeach
                </select>
            <input type="text" name="author_name" class="form-control">
        </div>
        <input type="submit" value="Add Book" class="btn btn-success">
    </form>

@endsection