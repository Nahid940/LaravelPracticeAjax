<?php

use Illuminate\Database\Seeder;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        factory(App\Datum::class, 50)->create()->each(function ($u) {
            $this->call(DataTableSeeder::class);
//            $u->posts()->save(factory(App\Post::class)->make());

        });
    }
}
