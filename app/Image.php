<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    use Uuids;
    public $incrementing=false;
    protected $fillable=['image'];
}
