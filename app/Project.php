<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $fillable =['pname'];
    public function projectImage(){
        return $this->hasMany(ProjectImage::class);
    }

}
