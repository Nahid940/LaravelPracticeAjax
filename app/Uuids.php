<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 10/28/2017
 * Time: 10:56 PM
 */

namespace App;

use Webpatser\Uuid\Uuid;


trait Uuids
{


    protected static function boot(){
        parent::boot();
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }

}