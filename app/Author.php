<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    //
//    public function books(){
//        return $this->belongsTo(Author::class);
//    }
    protected $fillable =['book_id','author_name'];

    public function book(){
        return $this->belongsTo(Book::class);
    }

}
