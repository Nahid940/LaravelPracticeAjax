<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    //
    use SoftDeletes;
    use Uuids;
    protected $fillable=['name','age'];
    protected $dates=['deleted_at'];
    public $incrementing=false;
}
