<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{
    //

    public function getPDF(){
        $pdf=PDF::loadview('admin.pdf.studentData');
        return $pdf->download('studentData.pdf');
    }
}
