<?php

namespace App\Http\Controllers;

use App\Course;
use App\Image;
use App\Person;
use App\Student;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $person=Person::all();
        $course=Course::all();
        $data=Image::select('image')->get();

        $student=Student::select('student_name','id')->where('assigned','not')->get();

        return view('admin/index',compact('person','course','student','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validateData=$this->validate($request,[
           'name'=>'required',
           'age'=>'required',
        ]);

        Person::create($validateData);
        return redirect('admin/')->with('insert','Data inserted !!');

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $person=Person::find($id);
        return view('admin/edit',compact('person'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validData=$this->validate($request,[
            'name'=>'required',
            'age'=>'required',

        ]);

        $person=Person::find($id);
        $person->update($validData);
        return redirect('admin/')->with('update','Data updated !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Person::destroy($id);
        return redirect('admin/')->with('deleted',"Data moved to trash !!");
    }

    public function trash()
    {
        $person=Person::onlyTrashed()->get();
        return view('admin/trash',compact('person'));
    }


    public function getTime(Request $request){
        $data=array();
        $data=[$request->input('time'),$request->input('date')];
        //$request->input('time');
        //$request->input('date);

        return $data;
    }
}
