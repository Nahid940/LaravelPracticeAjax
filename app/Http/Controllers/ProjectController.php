<?php

namespace App\Http\Controllers;

use App\Datum;
use App\Project;
use App\ProjectImage;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $project=Project::find(2)->projectImage;
//        return redirect()->route('project')->with('project',$project);
        return view('admin/project',compact('project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $id=Project::select('id')->orderBy('created_at','desc')->first();
        Project::create(['pname'=>$request->pname]);
        if($request->hasFile('image')){
            foreach ($request->image as $img){
                $name=$img->getClientOriginalName();
                $ext=$img->extension();
                $fileName=md5($name.time()).".".$ext;
                $img->storeAs('public/pimage',$fileName);
                ProjectImage::create(['project_id'=>'2','image'=>$fileName]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDataWithAjax(){
        $data=Datum::select('id','name')->offset(0)->limit(5)->get();
        $output='';
        foreach ($data as $d){
            $output.="<div class='alert alert-success'><i class='fa fa-smile-o' aria-hidden='true'></i> ".$d->name."
            <span class='click' data-id=$d->id><a style='cursor:pointer' id='like$d->id' data-id=$d->id><i class='fa fa-thumbs-o-up' aria-hidden='true'></i></a></span>
            </div>";
        }
        return $output;
    }

    public function datas(){
        $dataArray=array();
        $data=Datum::select('name')->get();

        foreach ($data as $d){
            $dataArray[]=$d->name;
        }
        return $dataArray;
    }
    public function sendName(Request $request){

        return $request->input('name');
    }
}
