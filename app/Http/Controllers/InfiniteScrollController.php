<?php

namespace App\Http\Controllers;

use App\Datum;
use Illuminate\Http\Request;

class InfiniteScrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datas=Datum::offset(0)->limit(10)->get();
        return view('admin/InfiniteScrollAjax',compact('datas'));
    }

    public function getData(Request $request){

        $SpanId=$request->SpanId;
        $output='';
        $datas=Datum::where('id','>=',$SpanId+1)->limit(10)->get();
        if(!$datas->isEmpty()){
        foreach ($datas as $d){
             $output.="<div><h3><a href=''>".$d['id'].' '.$d['name']."</a></h3><div class='text-right'><button class='btn btn-success'>Read More</button></div><hr style=\"margin-top:5px;\"></div>";
        }
        $output.="<span id='lastRow' data-id='.$d->id.' ></span>";
        $output.="<span id='loading'></span>";
        echo $output;
        }
}




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
