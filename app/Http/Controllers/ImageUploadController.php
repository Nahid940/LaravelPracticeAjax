<?php

namespace App\Http\Controllers;

use App\Image;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ImageUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->hasFile('image') && $request->file('image')->extension()==('jpeg'||'png')){
            $image=Input::file('image');
            $ext=$request->file('image')->extension();
//            echo $ext;
            $filename=md5($image->getClientOriginalName().time()).".".$ext;
            $request->file('image')->storeAs('public/image',$filename);

            Image::create(['image'=>$filename]);
        }else{

           echo  "no";
        }
    }

    public function multipleImage(Request $request){

        if($request->hasFile('image')) {

            foreach ($request->image as $img) {
                $name = $img->getClientOriginalName();
                $ext=$img->extension();
                $filename=md5($name.time()).".".$ext;
                $img->storeAs('public/image',$filename);
                Image::create(['image'=>$filename]);

            }


        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
