<?php

namespace App\Http\Controllers;

use App\Datum;
use Illuminate\Http\Request;
use Symfony\Component\VarDumper\Cloner\Data;

class LoadDataAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=Datum::offset(0)->limit(2)->get();
        return view('admin/loadData',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function load(Request $request){
        $output='';
        $id=$request->id;
        $start=$request->start;
        $limit=$request->limit;
        $data=Datum::where('id','>=',$id)->orderBy('created_at','asc')->limit(2)->get();

        if(!$data->isEmpty()){
            foreach ($data as $dt){
                $output.="<div class='alert alert-success'><p>".$dt->id.$dt->name."</p></div>";
            }
            $output.="<div id='remove-row'> <a class='btn btn-info' id='loadMoreData' data-id=".$dt->id." >Load more</a></div>";
            echo $output;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
