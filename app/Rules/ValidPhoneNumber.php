<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidPhoneNumber implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    protected $message;
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //

        if(!empty($value)){
            $non_digit=['*','-','#','%','@','^','&','(',')'];
            $number=str_replace($non_digit,' ',$value);

            if(!(is_numeric($value))){
                $this->message="Only numeric value is allowed !!";
                return false;
            }else if(strlen($number)<11 || strlen($number)>11){
                $this->message="Length exceeds !!";
                return false;
            }else if(substr($value,0,3)!='017'){
                $this->message="Only GP allowed !!";
                return false;
            }else{
                return true;
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
