<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    protected  $fillable=['bookName'];
    public function authors(){
        return $this->hasMany(Author::class);
    }
}
