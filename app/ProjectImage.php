<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectImage extends Model
{
    //

    protected $fillable =['image','project_id'];

    public function project(){
        return $this->belongsTo(Project::class);
    }


}
